<?php
require('vehicle.class.php');

class bycicle extends vehicle{
    private $nameB;
    private $speed;
    private $color;

    public function __construch($nm, $sp, $cl){
        $this->nameB = $nm;
        $this->speed = $sp;
        $this->color = $cl;

    }

    public function get_nameB(){
        return $this->nameB;
    }

    public function set_nameB($val){
        $this->nameB = $val;
    }

    public function get_speed(){
        return $this->speed;
    }

    public function set_speed($val){
        $this->speed = $val;
    }

    public function get_color(){
        return $this->color;
    }

    public function set_color($val){
        $this->color = $val;
    }
}
?>
