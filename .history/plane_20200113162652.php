<?php

require('transport.class.php');

class plane extends transport{
    private $namePlane;
    private $power;
    private $airCompany;

    public function __construct($nP, $pow , $aC){
        super();
        $this->namePlane = $nP;
        $this->power = $pow;
        $this->airCompany = $aC;
    }

    public function get_name_plane(){
        return $this->namePlane;
    }

    public function set_name_plane($val){
        $this->namePlane =$val;
    }

    public function get_power(){
        return $this->power;
    }

    public function set_power($val){
        $this->power = $val;
    }

    public function get_air_company(){
        return $this->airCompany;
    }

    public function set_air_company($val){
        $this->airCompany = $val;
    }
}
?>