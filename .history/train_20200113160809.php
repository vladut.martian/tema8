<?php
 require('transport.class.php');


 class train extends transport{
     private $nameTrain;
     private $typeOfTrain;
     private $maxSpeed;

     public function __construct($nT, $tTrn, $mS){
        $this->nameTrain = $nT;
        $this->typeOfTrain = $tTrn;
        $this->maxSpeed = $mS;
     }

     public function get_name_train(){
         return $this->nameTrain;
     }

     public function set_name_train($val){
        $this->nameTrain = $val;
     }

     public function get__type_of_train(){
         return $this->typeOfTrain;
     }

     public function set_type_of_train($val){
         $this->typeOfTrain = $val;
     }

     public function get_max_speed(){
         return $this->maxSpeed;
     }

     public function set_max_speed($val){
        $this->maxSpeed = $val;
     }
 }
?>