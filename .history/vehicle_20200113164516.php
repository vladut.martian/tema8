<?php

require('transport.class.php');

class vehicle extends transport{
    private $nameVehicle;
    private $nrOfWheels;

    public function __construct($nV,$nW){
        $this->nameVehicle = $nV;
        $this->nrOfWheels = $nW;

    }

    public  function get_name_vehicle(){
        return $this->nameVehicle;
    }

    public function set_name-vehicle($val){
        $this->nameVehicle = $val;
    }

    public function get_nr_of_wheels(){
        return $this->nrOfWheels;
    }

    public function set_nr_of_wheels($val){
        $this->nrOfWheels= $val;
    }

}
?>
