<?php
require('vehicle.class.php');

class car extends vehicle{
    private $name;
    private $horsePower;
    private $typeCar;

    public function __construct($nm, $hP, $tC){
        $this->name = $nm;
        $this->horsePower = $hP;
        $this->typeCar = $tC;
    }

    public function get_name(){
        return $this->name;
    }

    public function set_name($val){
        $this->name = $val;
    }

    public function get_horse_power(){
        return $this->horsePower;
    }

    public function set_horse_power($val){
        $this->horsePower = $val;
    }

    public function get_type_car(){
        return  $this->typeCar;
    }

    public function set_type_car(){
        $this->typeCar = $val;
    }
}
?>