<?php
class transport{
    private $typeTransport;
    private $dimensionTransport;
    private $modeTranport;
    

    public function__construct($tT,$dT,$mT){
        $this->typeTransport = $tT;
        $this->dimensionTransport = $tT;
        $this->modeTranport=$mT;
    }
    public function get_type_transport(){
        return $this->typeTransport;
    }

    public function set_type_transport($val){
        $this->typeTransport = $val;
    }

    public function get_dimension_transport(){
        return $this->dimensionTransport;
    }

    public function set_dimension_transport($val){
        $this->dimensionTransport=$val; 
    }

    public function get_mode_transport(){
        return $this->modeTransport;
    }

    public function set_mode_transport($val){
        $this->modeTransport = $val;
    }
}


?>